import Vue from 'vue'
import App from './App.vue'
import * as echarts from "echarts";
import antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts;
Vue.use(antd);
new Vue({
  render: h => h(App),
}).$mount('#app')
